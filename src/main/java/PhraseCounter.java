import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class PhraseCounter {
    public int getMinFrequency() {
        return minFrequency;
    }

    public void setMinFrequency(int minFrequency) {
        this.minFrequency = minFrequency;
    }

    public int getPhraseLength() {
        return phraseLength;
    }

    public void setPhraseLength(int phraseLength) {
        this.phraseLength = phraseLength;
    }

    private int minFrequency;
    private int phraseLength;
    HashMap<String, Integer> phraseFrequencies;

    public PhraseCounter(int newMinFrequency, int newPhraseLength){
        phraseFrequencies = new HashMap<String, Integer>();
        minFrequency = newMinFrequency;
        phraseLength = newPhraseLength;
    }

    public void countFrequencies(InputStream inputStream) {
        ArrayList<String> currentPhrase = new ArrayList<String>();
        String stringPhrase;
        try (Scanner scanner = new Scanner(inputStream))
        {
            for (int i = 0; i < (phraseLength - 1) && scanner.hasNext(); i++) {
                currentPhrase.add(scanner.next());
            }

            while (scanner.hasNext()) {
                currentPhrase.add(scanner.next());
                stringPhrase = String.join(" ", currentPhrase);
                //  phraseFrequencies.put("qwe", 1);
                phraseFrequencies.put(stringPhrase, phraseFrequencies.getOrDefault(stringPhrase, 0) + 1);
                currentPhrase.remove(0);
            }
        }
    }

    public HashMap<String, Integer> getPhraseFrequencies() {
        return phraseFrequencies;
    }

    public void printPhrases(OutputStream outputStream) {
        PrintStream writer = new PrintStream(outputStream);
        phraseFrequencies.entrySet().stream()
                .filter(phrase -> (phrase.getValue() >= minFrequency))
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(phrase -> writer.print(phrase.getKey() + " (" + phrase.getValue() + ")\n"));

    }

}
