import java.io.*;

public class Main {
    private static final String ARG_MIN_FREQ = "-m";
    private static final String ARG_PHRASE_LENGTH = "-n";
    private static final int DEFAULT_PHRASE_LENGTH = 2;
    private static final int DEFAULT_MIN_FREQ = 2;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        int minFreq = DEFAULT_MIN_FREQ, phraseLength = DEFAULT_PHRASE_LENGTH;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals(ARG_MIN_FREQ))
            {
                i++;
                minFreq = Integer.parseInt(args[i]);
            }
            else
            if (args[i].equals(ARG_PHRASE_LENGTH))
            {
                i++;
                phraseLength = Integer.parseInt(args[i]);
            }
            else {
                if (!args[i].equals("-"))
                    try {
                        inputStream = new FileInputStream(args[i]);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
            }
        }

        PhraseCounter phraseCounter = new PhraseCounter(minFreq, phraseLength);
        phraseCounter.countFrequencies(inputStream);
        phraseCounter.printPhrases(outputStream);
    }
}
