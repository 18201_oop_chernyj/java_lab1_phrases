import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.oneOf;

public class Tests {
    @Test
    public void generalTest() {
        PhraseCounter phraseCounter = new PhraseCounter(4, 3);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        String input = "we all live in a yellow submarine\n" +
                "yellow submarine yellow submarine\n" +
                "we all live in a yellow submarine\n" +
                "yellow submarine yellow submarine";
        phraseCounter.countFrequencies(new ByteArrayInputStream(input.getBytes()));
        phraseCounter.printPhrases(outputStream);
        assertThat(outputStream.toString(), oneOf(
                "submarine yellow submarine (4)\n" + "yellow submarine yellow (4)\n",
                "yellow submarine yellow (4)\n" + "submarine yellow submarine (4)\n"));
    }

    @Test
    public void countFrequenciesTest() {
        PhraseCounter phraseCounter = new PhraseCounter(1, 3);
        String input = "we all live in a yellow submarine\n" +
                "yellow submarine yellow submarine\n" +
                "we all live in a yellow submarine\n" +
                "yellow submarine yellow submarine";
        phraseCounter.countFrequencies(new ByteArrayInputStream(input.getBytes()));
        HashMap<String, Integer> frequencies = new HashMap<>((Map.of("yellow submarine we", 1,
                "submarine we all", 1,
                "in a yellow", 2,
                "a yellow submarine", 2,
                "we all live", 2,
                "all live in", 2,
                "live in a", 2,
                "yellow submarine yellow", 4,
                "submarine yellow submarine", 4)));
        Assertions.assertEquals(frequencies, phraseCounter.getPhraseFrequencies());
    }
}
